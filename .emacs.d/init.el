;;;; It must be noted that this file is a cluttered mess
;;;; Abandon all hope beyond this point
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") ; To be honest, I have no idea what this does and just saw it on the emacs subreddit as a fix for something
(package-initialize)
;; Fix exec-path
(add-to-list 'exec-path "/home/username/golib/bin")
;; packages
;; Systems packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  (eval-when-compile (require 'use-package)))
(use-package gnu-elpa-keyring-update
  :ensure t)
(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox-dark-soft t))
;; (use-package gruber-darker-theme
;;   :ensure t
;;   :config
;;   (load-theme 'gruber-darker t))
(use-package popwin
  :ensure t)
(use-package multiple-cursors
  :ensure t)
(use-package mood-line
  :ensure t
  :config (mood-line-mode))
(use-package theme-magic ; Export theme to pywal
  :ensure t
  :config
  (require 'theme-magic)
  (theme-magic-export-theme-mode))
(use-package ivy
  :ensure t
  :config
  ;; Get ivy extras
  (use-package swiper
    :ensure t)
  (use-package counsel
    :ensure t)
  ;; Configure ivy
  (ivy-mode 1)
  (setq ivy-height 9)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "[%d|%d] "))
(use-package org
  :ensure t)
(use-package org-bullets
  :ensure t)
(setq gc-cons-threshold 10000000)
(setq read-process-output-max (* 1024 1024))
;;; Development packagesp
(use-package go-mode
  :ensure t)
(use-package lsp-mode
  :ensure t
  :commands (lsp lsp-deferred)
  :hook (go-mode . lsp-deferred))
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)
(use-package lsp-ivy :ensure t :commands lsp-ivy-workspace-symbol)
(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (add-hook 'after-init-hook 'global-company-mode)
  (use-package slime-company
    :ensure t))
(use-package company-lsp
  :ensure t
  :commands company-lsp)
(use-package company-irony
  :ensure t
  :config
  (require 'company)
  (add-to-list 'company-backends 'company-irony))
(use-package irony
  :ensure t
  :config
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))
(with-eval-after-load 'company
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "M-p") nil)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous))
(use-package yasnippet
  :ensure t
  :config
  (use-package yasnippet-snippets
    :ensure t)
  (yas-reload-all))
(use-package projectile
  :ensure t)
(use-package project-explorer ;TODO: Remove, possibly?
  :ensure t)
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))
(use-package nix-mode ; Pretty much only used on NixOS
  :ensure t
  :mode "\\.nix\\'")
(use-package magit
  :ensure t)
(use-package slime
  :ensure t)
(use-package paredit
  :ensure t)
(use-package geiser
  :ensure t)
(use-package sml-mode
  :ensure t)
(use-package diminish ; Prevent cluttering of mode line via cluttering of init.el
  :ensure t
  :config
  (require 'diminish)
  (diminish 'theme-magic-export-theme-mode)
  (diminish 'yas-global-mode)
  (diminish 'ivy-mode))
;; Don't make stupid backup files
(setq make-backup-files nil)
;; Require
(require 'multiple-cursors)
(require 'dired-x)
(visual-line-mode) ; Visual Line Mode
;;; Word killing function I stole from something
(defun custom/kill-inner-word ()
  (interactive)
  (forward-char 1)
  (backward-word)
  (kill-word 1))
;; Bindings
(global-set-key (kbd "C-s")     'swiper-isearch)
(global-set-key (kbd "M-x")     'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "M-y")     'counsel-yank-pop)
(global-set-key (kbd "C-c M-c") 'mc/edit-lines)
(global-set-key (kbd "C-c C-z") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c C-x") 'mc/mark-previous-like-this)
;;(global-set-key (kbd "C-c C-e") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c v")   'enlarge-window)
(global-set-key (kbd "C-x g")   'magit-status)
(global-set-key (kbd "C-c r")   'project-manager-open)
(global-set-key (kbd "C-c w k") 'custom/kill-inner-word)
(global-unset-key (kbd "C-r"))
(global-unset-key (kbd "C-x c"))
(global-unset-key (kbd "C-x ^"))
;;; Fix yes or no query
(defalias 'yes-or-no-p 'y-or-n-p)
;;; Dired
(setq dired-recursive-copies 'always)
(setq dired-recursive-deletes 'top)
;;; org-mode
;; Org-Bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
;; Normal org customs
(setq org-hide-emphasis-markers t
      org-fontify-done-headline t
      org-hide-leading-stars t
      org-pretty-entities t
      org-odd-levels-only t)
;;;; Programming stuff
;; Projectile
(require 'projectile)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(projectile-mode +1)
(setq projectile-project-search-path '("~/dev/"))
;; CL
(slime-setup '(slime-fancy slime-quicklisp slime-asdf slime-company))
(setq inferior-lisp-program "sbcl")
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(defun override-slime-repl-bindings-with-paredit ()
  (define-key slime-repl-mode-map
    (read-kbd-macro paredit-backward-delete-key) nil))
(add-hook 'emacs-lisp-mode-hook (lambda ()
				  (setq indent-tabs-mode nil)
				  (paredit-mode +1)))
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'slime-repl-mode-hook (lambda ()
                                  (paredit-mode +1)
                                  (override-slime-repl-bindings-with-paredit)))
(defalias 'perl-mode 'cperl-mode) ;; Switch perl mode to cperl-mode
;;;; End programming things
;;; Popwin
(require 'popwin)
(popwin-mode 1)
;; Project-Manager Popwin
(push "*project-explorer*" popwin:special-display-config)
;; Slime Popwin
(push "*slime-apropos*" popwin:special-display-config)
(push "*slime-macroexpansion*" popwin:special-display-config)
(push "*slime-description*" popwin:special-display-config)
(push '("*slime-compilation*" :noselect t) popwin:special-display-config)
(push "*slime-xref*" popwin:special-display-config)
(push '(sldb-mode :stick t) popwin:special-display-config)
(push 'slime-repl-mode popwin:special-display-config)
(push 'slime-connection-list-mode popwin:special-display-config)
;; Fix the annoying bell
(setq ring-bell-function 'ignore)
;; remove tool bar, menu bar, scroll bar
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#0a0814" "#f2241f" "#67b11d" "#b1951d" "#4f97d7" "#a31db1" "#28def0" "#b2b2b2"])
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (gruvbox-dark-soft)))
 '(custom-safe-themes
   (quote
    ("939ea070fb0141cd035608b2baabc4bd50d8ecc86af8528df9d41f4d83664c6a" "aded61687237d1dff6325edb492bde536f40b048eab7246c61d5c6643c696b7f" "b89ae2d35d2e18e4286c8be8aaecb41022c1a306070f64a66fd114310ade88aa" "4cf9ed30ea575fb0ca3cff6ef34b1b87192965245776afa9e9e20c17d115f3fb" "4138944fbed88c047c9973f68908b36b4153646a045648a22083bd622d1e636d" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" "30289fa8d502f71a392f40a0941a83842152a68c54ad69e0638ef52f04777a4c" "1a212b23eb9a9bedde5ca8d8568b1e6351f6d6f989dd9e9de7fba8621e8ef82d" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default)))
 '(fci-rule-color "#f8fce8")
 '(frame-brackground-mode (quote dark))
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (quote
    ("#3b2b40b432a1" "#07ab45f64ce9" "#475733ea3554" "#1d623c04567f" "#2d5343d8332c" "#436f35f73166" "#0613413e597e")))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#5b7300" . 20)
     ("#007d76" . 30)
     ("#0061a8" . 50)
     ("#866300" . 60)
     ("#992700" . 70)
     ("#a00559" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#866300" "#992700" "#a7020a" "#a00559" "#243e9b" "#0061a8" "#007d76" "#5b7300")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(hl-paren-background-colors (quote ("#e8fce8" "#c1e7f8" "#f8e8e8")))
 '(hl-paren-colors (quote ("#40883f" "#0287c8" "#b85c57")))
 '(hl-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f"))))
 '(lsp-ui-doc-border "#93a1a1")
 '(mood-line-mode t)
 '(newsticker-url-list
   (quote
    (("unixporn" "https://www.reddit.com/r/unixporn/.rss" nil nil nil))))
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#5b7300" "#b3c34d" "#0061a8" "#2aa198" "#d33682" "#6c71c4")))
 '(package-selected-packages
   (quote
    (ujelly-theme solarized-theme gruvbox-theme lsp-ivy company-lsp lsp-ui lsp-mode go-mode gruber-darker plan9-theme pdf-tools company-irony counsel ivy slime-company geiser nix-mode diminish yasnippet-snippets company theme-magic sml-mode gruber-darker-theme markdown-mode project-explorer projectile multiple-cursors magit rainbow-mode org-bullets paredit slime spacemacs-theme use-package gnu-elpa-keyring-update popwin)))
 '(pdf-view-midnight-colors (quote ("#b2b2b2" . "#292b2e")))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(sml/active-background-color "#98ece8")
 '(sml/active-foreground-color "#424242")
 '(sml/inactive-background-color "#4fa8a8")
 '(sml/inactive-foreground-color "#424242")
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#ca7966832090")
     (60 . "#c05578c91534")
     (80 . "#b58900")
     (100 . "#a6088eed0000")
     (120 . "#9e3a91a60000")
     (140 . "#9628943b0000")
     (160 . "#8dc596ad0000")
     (180 . "#859900")
     (200 . "#76ef9b6045e8")
     (220 . "#6cd69ca95b9d")
     (240 . "#5f5f9e06701f")
     (260 . "#4c1a9f778424")
     (280 . "#2aa198")
     (300 . "#3002984eaf4d")
     (320 . "#2f6f93e8bae0")
     (340 . "#2c598f79c66f")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#a7020a" "#dc322f" "#5b7300" "#859900" "#866300" "#b58900" "#0061a8" "#268bd2" "#a00559" "#d33682" "#007d76" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "PfEd" :family "Luxi Mono")))))
