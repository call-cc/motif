print:
	pkg_add stow
	pkg_add motif

deploy:
	stow -t ~ .

.PHONY: print deploy
