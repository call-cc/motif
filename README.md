# My dotfiles

These are my motif window manager dotfiles. They are currently running on my OpenBSD system, and I quite like them.

## Deployment instructions

Run `doas make && doas make deploy`

This assumes you have Firefox and Emacs. It will install everything else for you.
